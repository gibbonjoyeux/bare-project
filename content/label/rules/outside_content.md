---
title: "Outside Content"
date: 2018-11-15T14:59:10+01:00
draft: false

rule: "Your website cannot compensate bare label image/video/... restrictions by
hosting it outside and linking it from your site."
---

### EXPLANATION

The **Bare Label** has a lot of **content restrictions** (no video, no heavy
images etc) and these restrictions **cannot be bypass** by **linking** heavy
content on your website and **hosting** it on another.

A good exemple would be a website with a youtube channel which posts its videos
links to its website. This kind of website cannot be **Bare Labelled** cause it
contains (even if it's indirectly) heavy media.
