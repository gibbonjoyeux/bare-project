---
title: "Bare Label Icon"
date: 2018-12-06T12:57:27+01:00
draft: false
layout: "icon"
---

{{<
	img
	title="bare label"
	src="images/logo-green-research-open.svg"
	width="150px"
>}}

### Label Options

If activated, one of these **options** means that the partner decided to do
more than what the **Bare Label** requires:

- &#x1f332;: Means that the website is **green hosted**.
- &#x269b;: Means that the partner does **research** or **development** to
	improve the project content with **informations** for users and developers
	or **code** / **programs** to help developers build **better websites**.
- &#x1f441;: Means that the website **code** is **open-source**.
