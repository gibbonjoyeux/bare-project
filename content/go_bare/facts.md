---
title: "Facts & Numbers"
date: 2018-11-02T20:01:08+01:00
draft: false

public: "everyone"
description: "Facts and numbers about technology environmental impact"
---

Here are some facts about technology pollution:

<table class=facts>
	<tr>
		<th colspan=2>
			Energetic consumption distribution on numerical technology
		</th>
	</tr>
	{{< facts_percent percent="11" info="TVs" >}}
	{{< facts_percent percent="17" info="Computers" >}}
	{{< facts_percent percent="11" info="Smartphones" >}}
	{{< facts_percent percent="6" info="Other" >}}
	{{< facts_percent percent="19" info="Data Centers" type="B" >}}
	{{< facts_percent percent="16" info="Network" type="B" >}}
	{{< facts_percent percent="20" info="Terminals" type="B" >}}
</table>
