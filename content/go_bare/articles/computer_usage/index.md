---
title: "Computer Usage"
date: 2018-11-07T11:26:53+01:00
draft: false

description: "What about having a simpler computer usage ?"
---

The computer is, for most of us, one of our main tool. We use it a lot. Here's
some advices to reduce your computer consumption. It's **eco-friendly** and
**cheaper**.
If your computer is a laptop, you'll win **time between charges** and that's
another good point !
Also, having a simpler computer usage is good for your computer. It requires
**less calculation** and can increase your computer **lifespan**.

* * *
### Some advices about your computer usage

#### No social network
This is a classical one. But as social networks manage a lot of **data** (which
is polluting and clearly not nice), **distract** you with **advertising**,
**video**, **useless articles** (which are all very consuming) and take you away
from their *original name* goal **"social"**. Avoid them is an important step
if you want to consume and pollute less.
#### Less streaming
As video streaming is a massive actor of the internet pollution, **stop** or
**reduce your video streaming time** can be a really good thing. Is watching a
video tutorial really usefull when you have a text tutorial ? Do you really need
to watch the last released blockbuster ?
It is a nice way to come back to **books** actually...
#### Low definition streaming
For stuff you *cannot* stop watching, doing it in low definition is also a good
option. Most of my friends listen music on youtube, this is not a good practice
but watching it in **144p** is always better than a useless HD definition.
#### Streaming VS downloading
If you know that you will listen this **song** more than one time, watch its
youtube video again and again is **a pure waste of resources**, it's power
consuming for you and datacenters. **Download** it is the really best option in
this case.
For **movies**, it's another story as you won't watch the movie ten times
(normally I mean), so streaming and downloading pollution **should be
equivalent**.
#### Favorite bar
As an ecosia/qwant/duckduckgo/google... **research can be an important source of
power consumption** (not for you but for the **datacenter** actually), having
each or your frequently visited websites registered into your **favorite bar**
(on your **browser** or simply **as shortcut on your desktop**) can **save
energy from being worthlessly consume** and **time** (for you) by going directly
on your site.

* * *
### Some advices about your computer configurations

These advices has been test on Windows, some of them should be available on
other systems but not all of them.
#### Always activate the saving battery mode
With this simple thing, you can save like **1-2h** of autonomy.
#### Reduce screen resolution
I tried to reduce my computer screen resolution to **800x600** (like an old 
computer) and my computer autonomy switched from **3-4h to 8-11h** which is
pretty cool. Of course, **800x600** resolution is really low and for some
applications, it can be an issue I think. But the only one which has been a real
issue for me was **web browsers**. It hurt your eyes, text is really big and
it's really hard to see anything. To solve this problem, you can simply change
your browser default zoom. Personnaly, I choose **70-75%**, it's nice and clear.
On **Opera**, **Chrome** and **Edge**, you can simply define a default zoom, but
on **Firefox**, you need (I think) an extension, mine is called "Fixed Zoom".
<br><br>**Ps**: You can also switch to **960x540** to keep the **16:9** ratio.
#### Unicolor Background
As the "Is a black pixel less consuming ?" question is still in debate and depends
on what kind of screen you have, I won't say anything about that. But I still
think that a simple unicolor background should consume less than an animated
one.
And even if I really loved my gorilla background image, I realized that I was
really rarely looking at it so I switched to a simple black background.
#### Luminosity
A simple option to reduce you computer consumption would be to reduce its
luminosity.
#### Optimize video for autonomy
In your parameters, for video reading, you have two choices, optimize for
**video quality** or optimize for **battery autonomy**. Of course, the autonomy
optimization consumes less.
#### Deactivate useless background synchronization
You can deactivate your automatic mail, google drive / dropbox etc
**synchronization**. For me, **manual synchronization** is way enough.
