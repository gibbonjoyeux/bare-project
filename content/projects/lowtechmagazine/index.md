---
title: "Low-tech Magazine"
date: 2018-11-12T17:08:21+01:00
draft: false

websiteUrl: "https://solar.lowtechmagazine.com"
creator: "Kris De Decker"
description: "Low-tech Magazine questions the belief in technological progress, and highlights the potential of past knowledge and technologies for designing a sustainable society"
contactName: "Kris De Decker"
contactEmail: "solar@lowtechmagazine.com"
status: "online"
icon: "&#x2190;"
---

> "Low-tech Magazine questions the belief in technological progress, and highlights
the potential of past knowledge and technologies for designing a sustainable
society."

* * *

As Low-tech Magazine is a solar-powered website, it sometimes goes offline.
