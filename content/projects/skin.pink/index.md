---
title: "Skin.pink"
date: 2018-11-04T10:15:00+01:00
draft: false

label: true
labelOptions: ["green", "research", "open"]
websiteUrl: "http://skin.pink"
creator: "Bare Necessities Project"
description: "An ethical and ecological erotic website"
contactName: "Sebastien Huertas"
contactEmail: "cactusfluo@laposte.net"
icon: "&#x1f44c;"
status: "work in progress"
---

As video streaming is a huge portion of the internet pollution, pornography can
certainly be considered as one of the biggest actor in this pollution.
That's why we decided to create an **ecological pornographic website**.
But as the **Bare Necessities Project** promotes a **sound** web, we felt forced
to work on a **more ethical** pornographic project.

Our first goal is to work with **feminist pornography platforms** and create
**compact size** but attractive images from their work. The next step would be 
to allow **consenting amateur** to submit their own pictures as some other
**ethical** pronography projects does.
