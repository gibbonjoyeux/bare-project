---
title: "Sources"
date: 2018-11-15T15:45:06+01:00
draft: false

icon: "&#x1f441;"
---

The **Bare Necessities Project** website is open-source, you can found its
source code [here](https://gitlab.com/cactusfluo/bare-project).
